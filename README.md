## Motivation

A short description of the motivation behind the creation and maintenance of the project. This should explain **why** the project exists.

## Synopsis

At the top of the file there should be a short introduction and/ or overview that explains **what** the project is. This description should match descriptions added for package managers (Gemspec, package.json, etc.)

## The latest version

You can find the latest version to ...

    git clone ...

## Code Example

5-line code snippet on how its used (if it's a library)

## Installation

Provide code examples and explanations of how to get the project.

## Tests

```bash
mvn test
```

## Contributors

* Fabien Arcellier

## License

A short snippet describing the license (MIT, Apache, etc.)
